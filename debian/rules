#!/usr/bin/make -f

# This may bring unexpected warnings/errors
undefine STARPU_DEVEL

DH_OPTIONS=-Bbuild

# These are the best versions that nvcc supports.
CONTRIB_GCC_VERSION  := 8
CONTRIB_CLANG_VERSION  := 8

ifeq ($(DEB_SOURCE),starpu-contrib)
# nvcc sometimes chokes on some gcc features
CC=cuda-gcc
CXX=cuda-g++
endif
OMPI_CC=$(CC)
export CC
export CXX
export OMPI_CC

export NVCCFLAGS = -Xcompiler -fPIC -Xcompiler -std=c++03

ifneq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
CONFIGURE_NODOC = --disable-build-doc
endif

DO_PACKAGES := $(shell dh_listpackages)
ifneq (,$(filter libstarpu-openmp-llvm-1.4-1t64,$(DO_PACKAGES)))
OPENMP_LLVM=1
else
OPENMP_LLVM=0
endif

ifeq ($(OPENMP_LLVM),1)
CONFIGURE_OPENMP_LLVM = --enable-openmp-llvm
endif

%:
	dh $@ $(DH_OPTIONS)

override_dh_auto_configure:
	dh_auto_configure $(DH_OPTIONS) -- \
		--enable-opengl-render \
		--enable-openmp \
		--enable-quick-check \
		--enable-mlr-system-blas \
		--enable-starpurm \
		--enable-starpurm-examples \
		--enable-maxnodes=16 \
		--enable-maxcpus=32 \
		--enable-maxnumanodes=4 \
		--enable-tcpip-master-slave \
		--docdir=/usr/share/doc/libstarpu-dev --disable-build-doc-pdf \
		$(CONFIGURE_OPENMP_LLVM) \
		$(CONFIGURE_NODOC)

override_dh_auto_test:
	-HOME=$$PWD DISPLAY= DEB_BUILD_OPTIONS=$(patsubst parallel=%,,$(DEB_BUILD_OPTIONS)) dh_auto_test $(DH_OPTIONS) -- -k
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	$(MAKE) -C build showcheck
endif

override_dh_auto_install:
	dh_auto_install
ifeq ($(OPENMP_LLVM),1)
	# Debian does symlink libomp into libdir
	chrpath -d $(CURDIR)/debian/tmp/usr/lib/*/starpu/examples/starpu_openmp_llvm/hello-task
endif

override_dh_auto_clean:
	# No idea why salsa CI is adding +x on libsocl-1.3-0.install and libstarpu-1.3-5.install (and no others...)
	ls -l debian/*.install
	chmod -x debian/*.install
	dh_auto_clean $(DH_OPTIONS)
	rm -fr doc/doxygen/starpu.pdf
	rm -fr doc/doxygen/html
	rm -fr .starpu
	rm -fr .nv

# Switch to main rules
sw-main:
	sed -i 's/^starpu-contrib /starpu /' debian/changelog
	sed -i '1s/^Source: starpu-contrib/Source: starpu/' debian/control
	sed -i 's/Package: libstarpu-contrib/Package: libstarpu/' debian/control
	sed -i 's/Package: starpu-contrib/Package: starpu/' debian/control
	sed -i 's/Package: libsocl-contrib/Package: libsocl/' debian/control
	sed -i '/Depends/s/ libstarpu-contrib/ libstarpu/g' debian/control
	sed -i '/Depends/s/ libsocl-contrib/ libsocl/g' debian/control
	sed -i 's/Replaces: libstarpu-contrib/Replaces: libstarpu/' debian/control
	sed -i 's/Replaces: libsocl-contrib/Replaces: libsocl/' debian/control
	sed -i 's/Replaces: starpu-contrib/Replaces: starpu/' debian/control
	sed -i 's/Breaks: libstarpu-contrib/Breaks: libstarpu/' debian/control
	sed -i 's/Breaks: libsocl-contrib/Breaks: libsocl/' debian/control
	sed -i 's/Breaks: starpu-contrib/Breaks: starpu/' debian/control
	sed -i '/^Conflicts:/s/ libstarpu/ libstarpu-contrib/g' debian/control
	sed -i '/^Conflicts:/s/ libsocl/ libsocl-contrib/g' debian/control
	sed -i '/^Conflicts:/s/ starpu/ starpu-contrib/g' debian/control
	sed -i 's/^	nvidia-cuda-toolkit-gcc/#	nvidia-cuda-toolkit-gcc/' debian/control
	sed -i 's_^Section: contrib/_Section: _' debian/control
	sed -i 's/contrib-contrib/contrib/g' debian/control debian/changelog
	sed -i 's/^ This "contrib" version/# This "contrib" version/' debian/control

# Switch to contrib rules
sw-contrib:
	sed -i 's/^starpu /starpu-contrib /' debian/changelog
	sed -i '1s/^Source: starpu/Source: starpu-contrib/' debian/control
	sed -i 's/Package: libstarpu/Package: libstarpu-contrib/' debian/control
	sed -i 's/Package: starpu/Package: starpu-contrib/' debian/control
	sed -i 's/Package: libsocl/Package: libsocl-contrib/' debian/control
	sed -i '/Depends/s/ libstarpu/ libstarpu-contrib/g' debian/control
	sed -i '/Depends/s/ libsocl/ libsocl-contrib/g' debian/control
	sed -i 's/Replaces: libstarpu/Replaces: libstarpu-contrib/' debian/control
	sed -i 's/Replaces: libsocl/Replaces: libsocl-contrib/' debian/control
	sed -i 's/Replaces: starpu/Replaces: starpu-contrib/' debian/control
	sed -i 's/Breaks: libstarpu/Breaks: libstarpu-contrib/' debian/control
	sed -i 's/Breaks: libsocl/Breaks: libsocl-contrib/' debian/control
	sed -i 's/Breaks: starpu/Breaks: starpu-contrib/' debian/control
	sed -i '/^Conflicts:/s/ libstarpu-contrib/ libstarpu/g' debian/control
	sed -i '/^Conflicts:/s/ libsocl-contrib/ libsocl/g' debian/control
	sed -i '/^Conflicts:/s/ starpu-contrib/ starpu/g' debian/control
	sed -i 's/#	nvidia-cuda-toolkit-gcc/	nvidia-cuda-toolkit-gcc/' debian/control
	sed -i 's_contrib/contrib/_contrib_g' debian/control debian/control
	sed -i 's/contrib-contrib/contrib/g' debian/control debian/changelog
	sed -i 's_^Section: \([^/]*\)$$_Section: contrib/\1_' debian/control
	sed -i 's/^# This "contrib" version/ This "contrib" version/' debian/control
